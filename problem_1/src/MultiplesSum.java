import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aria on 12/03/2017.
 */
public class MultiplesSum {
    public static void main(String[] args) {
        multipeList(3, 5, 1000);
        multipleSum(multipeList(3, 5, 1000));
    }
    private static ArrayList<Integer> multipeList(int a, int b, int range){
        ArrayList<Integer> list = new ArrayList<>();
        for(int i=0; i*a<range;i++){
            list.add(i*a);
        }
        for(int i=0; i*b<range;i++){
            if(!list.contains(i*b)){
                list.add(i*b);
            }
        }
        //System.out.println(list);
        return list;
    }
    private static int multipleSum(ArrayList<Integer> list){
        int sum = 0;
        for (Integer i: list
             ) {
            sum += i;
        }
        System.out.println(sum);
        return sum;
    }
}
