/**
 * Created by Aria on 12/03/2017.
 */
public class EvenFibonacci {
    private static void evenFib(int limit){
        long time = System.nanoTime();
        int b = 1;
        int c = 2 ,d;
        long sum = 0;
        while (c < limit){
            sum += c;
            d = b+(c<<0x01);
            c = d + b + c;
            b = d;
        }
        long time2 = System.nanoTime();
        System.out.println(sum);
        System.out.println(time2-time);
    }

    public static void main(String[] args) {
        evenFib(4000000);
        //fibonacci(4000000);
    }

}
